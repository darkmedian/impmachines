if(window.location.hash)
    setTimeout(function(){ navigateTo(window.location.hash); }, 500);
$('.img-holder').imageScroll({
 parallax: true,
});
tp = typeof $("a");
function navigateTo(evt){
    var elem = (typeof evt=='object')?evt:$("a[href='"+evt+"']");
    var src  = elem.attr('href'),
    offset  = $(src).offset().top;
    $('html, body').stop().animate({
        scrollTop : offset
    },1000,
    function (){
        location.hash = src;
    }
    );
}
$('a').on('click', function (event) {
    event.preventDefault();
    navigateTo($(this));
});